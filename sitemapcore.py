import requests
import json
import sys
from bs4 import BeautifulSoup


def recursive_node_to_dict(nod):
	"""create a dict from a tree"""

	result = {
		'url': nod.url,
		'title': nod.name
		}
	if len(nod.children)>0:
		children = []
		for c in nod.children:
			children.append(recursive_node_to_dict(c))
		result['children'] = children
	return result

def find(x, root):
	"""Performs find if exist node with the same url in the tree"""
	if root.url == x:
		return True
	for nod in root.children:
		if find(x, nod):
			return True
	return False

class node:

	""" Class node

    Class to keep n-tree of links inside of the website

    Attributes:
        children: Array of nodes that born from this node.
        url: URL of this node
        name: title of this website
        domain: url root
        json: json generated from this node
    """

	children = []
	url = ""
	name = ""
	domain= ""
	json = ""

	def __init__(self, url, domain, title):
		"""Inits nodeClass with localURLm DomainURLm and title of node."""
		self.url = url
		self.domain = domain
		self.name = title
		self.children = []
		self.json = ""

	def run(self):
		"""Performs operation to get tree and generate json in the node"""
		print("This process can take some minutes :)")
		self.find_children(self)
		self.to_json()
		with open('out.json', 'w') as outfile:
			json.dump(self.json, outfile)
		print(self.json)

	def add_child(self, child_url, domain, title):
		"""Performs operation of add a new child to children array."""
		child = node(child_url, domain, title)
		self.children.append(child)

	def find_children(self, root):

		"""Performs recursive operation get all links check 
		if they are inside the domain and if they not exist in the tree,
		this fucntio is recursive in all children of this node"""

		headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
		content = requests.get(self.url, headers=headers).text
		soup = BeautifulSoup(content, "html.parser")
		links = soup.find_all("a", href=True)
		#print(self.url)
		title = soup.find("title").text
		#print(title)

		for link in links:
			url = link['href']
			if not "#" in url:
				if not url.startswith("mailto") and not url.startswith("http") and not url.startswith("www."):
					if url[0] == '/':
						url = url[1:]
					url = self.domain + url
					if self.domain in url:
						if not find(url, root):
							self.add_child(url,self.domain, title)
		for child in self.children:
			child.find_children(root)
	
	def to_json(self):
		"""Performs operation of generate json in the same node"""
		self.json = json.dumps(recursive_node_to_dict(self))

if __name__ == '__main__':
	url = "https://www.warnerbros.com/archive/spacejam/movie/"
	if len(sys.argv)>1:
		url = sys.argv[1]
		if not url[len(url)-1]=='/':
			url = url + "/"
		print("Using url: "+url)	
	else:
		print("Using default url: "+url)
	node(url, url, "root").run()
